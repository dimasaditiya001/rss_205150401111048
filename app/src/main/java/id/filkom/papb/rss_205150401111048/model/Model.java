package id.filkom.papb.rss_205150401111048.model;

public class Model {
    private String title;
    private String des;
    private String category;

    public Model(String title, String des, String category) {
        this.title = title;
        this.des = des;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public String getDes() {
        return des;
    }

    public String getCategory() {
        return category;
    }

}

